## Fuse REST API

> use ` pip install -r requirements.txt ` to install all dependencies

> Use `cd <Directory_Name>` eg. `cd "REST API using SQLALCHEMY"`

> Use ` flask run ` to start server

Main endpoint : ` https://localhost:5000`


### Tables in database

| Table   | Main Point | Attachable                                                                                                | Attributes                                                |
| ------- | ---------- | --------------------------------------------------------------------------------------------------------- | --------------------------------------------------------- |
| User    | /user      | / (GET,POST) <br> /:id (GET,PUT,DELETE)                                                                   | name,email,password                                       |
| Post    | /post      | / (GET,POST) <br>/:id (GET,PUT,DELETE) <br> /user/:user_id (GET,DELETE)                                   | content, posted_by,<br> posted_on(DEFAULT NOW)            |
| Comment | /comment   | / (GET,POST) <br>/:id (GET,PUT,DELETE) <br> /user/:user_id (GET,DELETE)  <br> /post/:post_id (GET,DELETE) | comment, commented_by,post, <br>commented_on(DEFAULT NOW) |

##### Examples

```py
https://localhost:5000/user/ returns all users in database
https://localhost:5000/post/user/:user_id returns all posts for a user
https://localhost:5000/comment/post/:user_id returns all comments for a post
```
