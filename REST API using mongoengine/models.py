import datetime
from app import db


class User(db.Document):
    name = db.StringField(required=True)
    email = db.StringField(required=True, unique=True)
    password = db.StringField(required=True)


class Post(db.Document):
    content = db.StringField(required=True)
    posted_on = db.DateTimeField(default=datetime.datetime.utcnow)
    posted_by = db.ReferenceField(User)


class Comment(db.Document):
    comment = db.StringField(required=True)
    commented_on = db.DateTimeField(default=datetime.datetime.utcnow)
    commented_by = db.ReferenceField(User)
    post = db.ReferenceField(Post)
