from app import app, request
from controllers import comment_controller


@app.route('/comment', methods=["GET", "POST"])
def comments():
    if(request.method == "GET"):
        comments = comment_controller.get_comments()
        return comments

    if (request.method == "POST"):
        comment_details = request.get_json()
        comment = comment_controller.insert_comment(
            comment_details)
        return comment


@app.route('/comment/<id>', methods=["GET", "PUT", "DELETE"])
def single_comment_routes(id):
    if (request.method == "GET"):
        comment = comment_controller.get_comment_by_id(id)
        return comment

    if(request.method == "PUT"):
        comment_details = request.get_json()
        comment = comment_controller.update_comment(
            id, comment_details)
        return comment

    if(request.method == "DELETE"):
        comment = comment_controller.delete_comment(id)
        return comment


@app.route('/comment/user/<user_id>', methods=["GET", "DELETE"])
def comments_for_user_routes(user_id):
    if (request.method == "GET"):
        comments_for_user = comment_controller.get_comments_for_user(user_id)
        return comments_for_user

    if(request.method == "DELETE"):
        comment = comment_controller.delete_comments_for_user(user_id)
        return comment


@app.route('/comment/post/<post_id>', methods=["GET", "DELETE"])
def comments_for_post_routes(post_id):
    if (request.method == "GET"):
        comments_for_post = comment_controller.get_comments_for_post(post_id)
        return comments_for_post

    if(request.method == "DELETE"):
        comment = comment_controller.delete_comments_for_post(post_id)
        return comment
