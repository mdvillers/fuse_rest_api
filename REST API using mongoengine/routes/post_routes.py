from app import app, request
from controllers import post_controller


@app.route('/post', methods=["GET", "POST"])
def posts():
    if(request.method == "GET"):
        posts = post_controller.get_posts()
        return posts

    if (request.method == "POST"):
        post_details = request.get_json()
        post = post_controller.insert_post(post_details)
        return post


@app.route('/post/<id>', methods=["GET", "PUT", "DELETE"])
def single_post_routes(id):
    if (request.method == "GET"):
        post = post_controller.get_post_by_id(id)
        return post

    if(request.method == "PUT"):
        post_details = request.get_json()

        post = post_controller.update_post(id, post_details)
        return post

    if(request.method == "DELETE"):
        post = post_controller.delete_post(id)
        return post


@app.route('/post/user/<user_id>', methods=["GET", "DELETE"])
def posts_for_user_routes(user_id):
    if (request.method == "GET"):
        posts_for_user = post_controller.get_posts_for_user(user_id)
        return posts_for_user

    if(request.method == "DELETE"):
        post = post_controller.delete_posts_for_user(user_id)
        return post
