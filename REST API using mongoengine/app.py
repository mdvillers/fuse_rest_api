from flask import Flask, jsonify, request
from flask_mongoengine import MongoEngine
from flask_marshmallow import Marshmallow


app = Flask(__name__)
app.config.from_pyfile('config.cfg')
db = MongoEngine(app)

from routes.comment_routes import *
from routes.post_routes import *
from routes.user_routes import *

if __name__ == '__main__':
    # db.create_all()
    app.run(port=5000, debug=True)
