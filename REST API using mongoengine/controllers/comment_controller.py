from app import jsonify
from models import Post, User, Comment
from bson.objectid import ObjectId


def insert_comment(comment_details):

    user_id = comment_details["commented_by"]
    post_id = comment_details["post"]

    if(not ObjectId.is_valid(user_id)):
        return jsonify("Not a valid user id"), 400

    if(not ObjectId.is_valid(post_id)):
        return jsonify("Not a valid post id"), 400

    user = User.objects(id=ObjectId(user_id)).first_or_404(
        "commented_by: Not a valid userid")

    post = Post.objects(id=ObjectId(post_id)).first_or_404(
        "commented_by: Not a valid postid")

    comment = Comment(**comment_details).save()
    return jsonify(comment)


def get_comments():
    comments = Comment.objects()
    return jsonify(comments)


def get_comment_by_id(id):
    if(not ObjectId.is_valid(id)):
        return jsonify("Not a valid comment id"), 400
    # comment = Comment.objects(id=id).first()
    comment = Comment.objects(id=id).first_or_404()
    return jsonify(comment)


def update_comment(id, comment_details):

    comment = Comment.objects(id=id).first_or_404()

    user_id = comment_details["commented_by"]
    post_id = comment_details["post"]

    if(not ObjectId.is_valid(user_id)):
        return jsonify("Not a valid user id"), 400

    if(not ObjectId.is_valid(post_id)):
        return jsonify("Not a valid post id"), 400

    user = User.objects(id=ObjectId(user_id)).first_or_404(
        "commented_by: Not a valid userid")

    post = Post.objects(id=ObjectId(post_id)).first_or_404(
        "commented_by: Not a valid postid")

    comment.update(commented_by=user, post=post,
                   comment=comment_details["comment"])
    return jsonify({"updated": str(comment.id)})


def delete_comment(id):
    if(not ObjectId.is_valid(id)):
        return jsonify("Not a valid comment id"), 400
    comment = Comment.objects(id=id).first_or_404()
    comment.delete()
    return jsonify({"deleted": str(comment.id)})


def get_comments_for_user(user_id):
    if(not ObjectId.is_valid(user_id)):
        return jsonify("Not a valid user id"), 400
    comments_for_users = Comment.objects(commented_by=user_id)
    return jsonify(comments_for_users)


def delete_comments_for_user(user_id):
    if(not ObjectId.is_valid(user_id)):
        return jsonify("Not a valid user id"), 400
    comments_for_users = Comment.objects(commented_by=user_id)
    comments_for_users.delete()
    return jsonify("deleted")


def get_comments_for_post(post_id):
    if(not ObjectId.is_valid(post_id)):
        return jsonify("Not a valid post id"), 400
    comments_for_posts = Comment.objects(post=post_id)
    return jsonify(comments_for_posts)


def delete_comments_for_post(post_id):
    if(not ObjectId.is_valid(post_id)):
        return jsonify("Not a valid post id"), 400
    comments_for_posts = Comment.objects(post=post_id)
    comments_for_posts.delete()
    return jsonify("deleted")
