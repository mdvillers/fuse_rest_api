from app import jsonify
from models import Post, User, Comment
from bson.objectid import ObjectId


def insert_post(post_details):
    user_id = post_details["posted_by"]
    if(not ObjectId.is_valid(user_id)):
        return jsonify("Not a valid user id"), 400

    user = User.objects(id=ObjectId(user_id)).first_or_404(
        "posted_by: Not a valid userid")
    post = Post(**post_details).save()
    return jsonify(post)


def get_posts():
    posts = Post.objects()
    return jsonify(posts)


def get_post_by_id(id):
    if(not ObjectId.is_valid(id)):
        return jsonify("Not a valid post id"), 400
    # post = Post.objects(id=id).first()
    post = Post.objects(id=id).first_or_404()
    return jsonify(post)


def update_post(id, post_details):

    if(not ObjectId.is_valid(id)):
        return jsonify("Not a valid post id"), 400

    post = Post.objects(id=id).first_or_404()
    user_id = post_details["posted_by"]

    if(not ObjectId.is_valid(user_id)):
        return jsonify("Not a valid user id"), 400

    user = User.objects(id=ObjectId(user_id)).first_or_404(
        "posted_by: Not a valid userid")

    post.update(content=post_details["content"], posted_by=user)

    return jsonify({"updated": str(post.id)})


def delete_post(id):
    if(not ObjectId.is_valid(id)):
        return jsonify("Not a valid post id"), 400
    post = Post.objects(id=id).first_or_404()

    comments_for_post = Comment.objects(post=post)
    post.delete()
    comments_for_post.delete()

    return jsonify({"deleted": str(post.id)})


def get_posts_for_user(user_id):
    if(not ObjectId.is_valid(user_id)):
        return jsonify("Not a valid user id"), 400
    posts_for_users = Post.objects(posted_by=user_id)
    return jsonify(posts_for_users)


def delete_posts_for_user(user_id):
    if(not ObjectId.is_valid(user_id)):
        return jsonify("Not a valid user id"), 400
    posts_for_users = Post.objects(posted_by=user_id)

    for post in posts_for_users:
        comments_for_post = Comment.objects(post=post)
        comments_for_post.delete()

    posts_for_users.delete()

    return jsonify("deleted")
