from app import jsonify
from models import User, Post, Comment
from bson.objectid import ObjectId


def insert_user(user_details):
    user = User(**user_details).save()
    return jsonify(user)


def get_users():
    users = User.objects()
    return jsonify(users)


def get_user_by_id(id):
    if(not ObjectId.is_valid(id)):
        return jsonify("Not a valid user id"), 400

    # user = User.objects(id=id).first()
    user = User.objects(id=id).first_or_404("User Not Found")
    return jsonify(user)


def update_user(id, user_details):
    if(not ObjectId.is_valid(id)):
        return jsonify("Not a valid user id"), 400

    user = User.objects(id=id).first_or_404("User not found")
    user.update(**user_details)
    return jsonify({"updated": str(user.id)})


def delete_user(id):
    if(not ObjectId.is_valid(id)):
        return jsonify("Not a valid user id"), 400

    user = User.objects(id=id).first_or_404("User not found")

    posts_by_user = Post.objects(posted_by=user)
    comments_by_user = Comment.objects(commented_by=user)

    user.delete()
    posts_by_user.delete()
    comments_by_user.delete()
    
    return jsonify({"deleted": str(user.id)})
