from flask import Flask,jsonify,request
import sqlite3
import db

app = Flask(__name__)

app.config.from_pyfile('config.cfg')


from routes.user_routes import *
from routes.post_routes import *
from routes.comment_routes import *

   

if __name__=='__main__':
    db.create_tables()
    print('tables')
    app.run(port=5000,debug=True)