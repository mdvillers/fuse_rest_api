from app import app, request, jsonify
from controllers import user_controller


@app.route('/user', methods=["GET", "POST"])
def users():
    if(request.method == "GET"):
        users = user_controller.get_users()
        return jsonify(users)
    if (request.method == "POST"):
        user_details = request.get_json()
        name = user_details["name"]
        email = user_details["email"]
        password = user_details["password"]
        user = user_controller.insert_user(name, email, password)
        return(jsonify(user))


@app.route('/user/<int:id>', methods=["GET", "PUT", "DELETE"])
def single_user_routes(id):

    if (request.method == "GET"):
        user = user_controller.get_user_by_id(id)
        return jsonify(user)

    if(request.method == "PUT"):
        user_details = request.get_json()
        name = user_details["name"]
        email = user_details["email"]
        password = user_details["password"]
        message = user_controller.update_user(id, name, email, password)
        return jsonify(message)

    if(request.method == "DELETE"):
        message = user_controller.delete_user(id)
        return jsonify(message)
