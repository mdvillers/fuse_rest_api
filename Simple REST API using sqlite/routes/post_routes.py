from app import app, request, jsonify
from controllers import post_controller


@app.route('/post', methods=["GET", "POST"])
def posts():
    if(request.method == "GET"):
        posts = post_controller.get_posts()
        return jsonify(posts)

    if (request.method == "POST"):
        post_details = request.get_json()
        content = post_details["content"]
        posted_by = post_details["posted_by"]
        post = post_controller.insert_post(content, posted_by)
        return(jsonify(post))


@app.route('/post/<int:id>', methods=["GET", "PUT", "DELETE"])
def single_post_routes(id):
    if (request.method == "GET"):
        post = post_controller.get_post_by_id(id)
        return jsonify(post)

    if(request.method == "PUT"):
        post_details = request.get_json()
        content = post_details["content"]
        posted_by = post_details["posted_by"]
        message = post_controller.update_post(id, content, posted_by)
        return jsonify(message)

    if(request.method == "DELETE"):
        message = post_controller.delete_post(id)
        return jsonify(message)


@app.route('/post/user/<int:user_id>', methods=["GET", "DELETE"])
def posts_for_user_routes(user_id):
    if (request.method == "GET"):
        posts_for_user = post_controller.get_posts_for_user(user_id)
        return jsonify(posts_for_user)

    if(request.method == "DELETE"):
        message = post_controller.delete_posts_for_user(user_id)
        return jsonify(message)
