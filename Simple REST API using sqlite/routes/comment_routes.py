from app import app, request, jsonify
from controllers import comment_controller


@app.route('/comment', methods=["GET", "POST"])
def comments():
    if(request.method == "GET"):
        comments = comment_controller.get_comments()
        return jsonify(comments)

    if (request.method == "POST"):
        comment_details = request.get_json()
        comment = comment_details["comment"]
        commented_by = comment_details["commented_by"]
        post = comment_details["post"]
        comment = comment_controller.insert_comment(
            comment, commented_by, post)
        return(jsonify(comment))


@app.route('/comment/<int:id>', methods=["GET", "PUT", "DELETE"])
def single_comment_routes(id):
    if (request.method == "GET"):
        comment = comment_controller.get_comment_by_id(id)
        return jsonify(comment)

    if(request.method == "PUT"):
        comment_details = request.get_json()
        comment = comment_details["comment"]
        commented_by = comment_details["commented_by"]
        post = comment_details["post"]
        message = comment_controller.update_comment(
            id, comment, commented_by, post)
        return jsonify(message)

    if(request.method == "DELETE"):
        message = comment_controller.delete_comment(id)
        return jsonify(message)


@app.route('/comment/user/<int:user_id>', methods=["GET", "DELETE"])
def comments_for_user_routes(user_id):
    if (request.method == "GET"):
        comments_for_user = comment_controller.get_comments_for_user(user_id)
        return jsonify(comments_for_user)

    if(request.method == "DELETE"):
        message = comment_controller.delete_comments_for_user(user_id)
        return jsonify(message)


@app.route('/comment/post/<int:post_id>', methods=["GET", "DELETE"])
def comments_for_post_routes(post_id):
    if (request.method == "GET"):
        comments_for_post = comment_controller.get_comments_for_post(post_id)
        return jsonify(comments_for_post)

    if(request.method == "DELETE"):
        message = comment_controller.delete_comments_for_post(post_id)
        return jsonify(message)
