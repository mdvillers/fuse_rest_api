from db import get_db

def insert_post(content,posted_by):
    db=get_db()
    cursor=db.cursor()
    cursor.execute('PRAGMA foreign_keys = ON')
    query="INSERT INTO post (content,posted_by) VALUES (?,?)"
    cursor.execute(query,[content,posted_by])
    db.commit()
    return True


def get_posts():
    db=get_db()
    cursor=db.cursor()
    query="SELECT * FROM post"
    cursor.execute(query)
    return cursor.fetchall()

def get_post_by_id(id):
    db=get_db()
    cursor=db.cursor()
    query="SELECT * FROM post where id=?"
    cursor.execute(query,[id])
    return cursor.fetchone()

def get_posts_for_user(user_id):
    db=get_db()
    cursor=db.cursor()
    query="SELECT * FROM post where posted_by=?"
    cursor.execute(query,[user_id])
    return cursor.fetchall()

def update_post(id,content,posted_by):
    db=get_db()
    cursor=db.cursor()
    cursor.execute('PRAGMA foreign_keys = ON')
    statement = "UPDATE post SET content = ?, posted_by = ? WHERE id = ?"
    cursor.execute(statement, [content,posted_by, id])
    db.commit()
    return True

def delete_post(id):
    db=get_db()
    cursor=db.cursor()
    cursor.execute('PRAGMA foreign_keys = ON')
    query="DELETE FROM post where id=?"
    cursor.execute(query,[id])
    db.commit()
    return True


def delete_posts_for_user(user_id):
    db=get_db()
    cursor=db.cursor()
    cursor.execute('PRAGMA foreign_keys = ON')
    query="DELETE FROM post where posted_by=?"
    cursor.execute(query,[user_id])
    db.commit()
    return True