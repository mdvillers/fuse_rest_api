from db import get_db

def insert_comment(comment,commented_by,post):
    db=get_db()
    cursor=db.cursor()
    cursor.execute('PRAGMA foreign_keys = ON')
    query="INSERT INTO comment (comment,commented_by,post) VALUES (?,?,?)"
    cursor.execute(query,[comment,commented_by,post])
    db.commit()
    return True


def get_comments():
    db=get_db()
    cursor=db.cursor()
    query="SELECT * FROM comment"
    cursor.execute(query)
    return cursor.fetchall()

def get_comment_by_id(id):
    db=get_db()
    cursor=db.cursor()
    query="SELECT * FROM comment where id=?"
    cursor.execute(query,[id])
    return cursor.fetchone()



def update_comment(id,comment,commented_by,post):
    db=get_db()
    cursor=db.cursor()
    cursor.execute('PRAGMA foreign_keys = ON')
    statement = "UPDATE comment SET comment = ?, commented_by = ?, post=? WHERE id = ?"
    cursor.execute(statement, [comment,commented_by,post, id])
    db.commit()
    return True

def delete_comment(id):
    db=get_db()
    cursor=db.cursor()
    cursor.execute('PRAGMA foreign_keys = ON')
    query="DELETE FROM comment where id=?"
    cursor.execute(query,[id])
    db.commit()
    return True


def get_comments_for_user(user_id):
    db=get_db()
    cursor=db.cursor()
    query="SELECT * FROM comment where commented_by=?"
    cursor.execute(query,[user_id])
    return cursor.fetchall()

def delete_comments_for_user(user_id):
    db=get_db()
    cursor=db.cursor()
    cursor.execute('PRAGMA foreign_keys = ON')
    query="DELETE FROM comment where commented_by=?"
    cursor.execute(query,[user_id])
    db.commit()
    return True

def get_comments_for_post(post_id):
    db=get_db()
    cursor=db.cursor()
    query="SELECT * FROM comment where post=?"
    cursor.execute(query,[post_id])
    return cursor.fetchall()

def delete_comments_for_post(post_id):
    db=get_db()
    cursor=db.cursor()
    cursor.execute('PRAGMA foreign_keys = ON')
    query="DELETE FROM comment where post=?"
    cursor.execute(query,[post_id])
    db.commit()
    return True