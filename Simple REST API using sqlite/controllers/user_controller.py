from db import get_db

def insert_user(name,email,password):
    db=get_db()
    cursor=db.cursor()
    query="INSERT INTO user (name,email,password) VALUES (?,?,?)"
    cursor.execute(query,[name,email,password])
    db.commit()
    return True


def get_users():
    db=get_db()
    cursor=db.cursor()
    query="SELECT id,name,email FROM user"
    cursor.execute(query)
    return cursor.fetchall()

def get_user_by_id(id):
    db=get_db()
    cursor=db.cursor()
    query="SELECT id,name,email FROM user where id=?"
    cursor.execute(query,[id])
    return cursor.fetchone()

def update_user(id,name,email,password):
    db=get_db()
    cursor=db.cursor()
    statement = "UPDATE user SET name = ?, email = ?, password = ? WHERE id = ?"
    cursor.execute(statement, [name, email, password, id])
    db.commit()
    return True

def delete_user(id):
    db=get_db()
    cursor=db.cursor()
    cursor.execute('PRAGMA foreign_keys = ON')
    query="DELETE FROM user where id=?"
    cursor.execute(query,[id])
    db.commit()
    return True