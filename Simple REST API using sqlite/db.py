import sqlite3
DB_NAME='test.db'

def get_db():
    conn = sqlite3.connect(DB_NAME)
    return conn

def create_tables():
    db=get_db()
    with open('schema.sql') as f:
       db.executescript(f.read())
    