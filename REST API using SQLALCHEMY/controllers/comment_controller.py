from app import db, jsonify
from models import Comment, User, Post, comment_schema, comments_schema


def insert_comment(comment, commented_by, post):
    new_comment = Comment(comment, commented_by, post)
    db.session.add(new_comment)
    db.session.commit()
    return comment_schema.jsonify(new_comment)


def get_comments():
    all_comments = Comment.query.all()
    result = comments_schema.dump(all_comments)
    return jsonify(result)


def get_comment_by_id(id):
    comment = Comment.query.get(id)
    return comment_schema.jsonify(comment)


def update_comment(id, comment, commented_by, post):
    comment = Comment.query.get(id)
    comment.comment = comment
    comment.commented_by = commented_by
    comment.post = post
    db.session.commit()
    return comment_schema.jsonify(comment)


def delete_comment(id):
    comment = Comment.query.get(id)
    db.session.delete(comment)
    db.session.commit()
    return comment_schema.jsonify(comment)


def get_comments_for_user(user_id):
    user = User.query.get(user_id)
    comments_for_user = comments_schema.dump(user.comments)
    return jsonify(comments_for_user)


def delete_comments_for_user(user_id):
    user = User.query.get(user_id)
    for comment in user.comments:
        db.session.delete(comment)
    db.session.commit()
    return jsonify({"message": "deleted"})


def get_comments_for_post(post_id):
    post = Post.query.get(post_id)
    comments_for_post = comments_schema.dump(post.comments)
    return jsonify(comments_for_post)


def delete_comments_for_post(post_id):
    post = Post.query.get(post_id)
    for comment in post.comments:
        db.session.delete(comment)
    db.session.commit()
    return jsonify({"message": "deleted"})
