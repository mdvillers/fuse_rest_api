from app import db, jsonify
from models import Post, User, post_schema, posts_schema


def insert_post(content, posted_by):
    new_post = Post(content, posted_by)
    db.session.add(new_post)
    db.session.commit()
    return post_schema.jsonify(new_post)


def get_posts():
    all_posts = Post.query.all()
    result = posts_schema.dump(all_posts)
    return jsonify(result)


def get_post_by_id(id):
    post = Post.query.get(id)
    return post_schema.jsonify(post)


def update_post(id, content, posted_by):
    post = Post.query.get(id)
    post.content = content
    post.posted_by = posted_by
    db.session.commit()
    return post_schema.jsonify(post)


def delete_post(id):
    post = Post.query.get(id)
    db.session.delete(post)
    db.session.commit()
    return post_schema.jsonify(post)


def get_posts_for_user(user_id):
    user = User.query.get(user_id)
    posts_for_user = posts_schema.dump(user.posts)
    return jsonify(posts_for_user)


def delete_posts_for_user(user_id):
    user = User.query.get(user_id)
    for post in user.posts:
        db.session.delete(post)
    db.session.commit()
    return jsonify({"message": "deleted"})
