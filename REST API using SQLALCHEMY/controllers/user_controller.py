from app import db, jsonify
from models import User, user_schema, users_schema


def insert_user(name, email, password):
    new_user = User(name, email, password)
    db.session.add(new_user)
    db.session.commit()
    return user_schema.jsonify(new_user)


def get_users():
    all_users = User.query.all()
    result = users_schema.dump(all_users)
    return jsonify(result)


def get_user_by_id(id):
    user = User.query.get(id)
    return user_schema.jsonify(user)


def update_user(id, name, email, password):
    user = User.query.get(id)
    user.name = name
    user.email = email
    user.password = password
    db.session.commit()
    return user_schema.jsonify(user)


def delete_user(id):
    user = User.query.get(id)
    db.session.delete(user)
    db.session.commit()
    return user_schema.jsonify(user)
