from os import name
from app import db, ma
from datetime import datetime

# models


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(100), nullable=False)
    posts = db.relationship('Post', backref='user',
                            cascade="all,delete", lazy=True)
    comments = db.relationship(
        'Comment', backref='user', cascade="all,delete", lazy=True)

    def __init__(self, name, email, password):
        self.name = name
        self.email = email
        self.password = password

    def __repr__(self):
        return f'User {self.name}'


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    content = db.Column(db.String(300), nullable=False)
    posted_on = db.Column(db.DateTime, default=datetime.utcnow)
    posted_by = db.Column(db.Integer, db.ForeignKey('user.id', ondelete="CASCADE", onupdate="CASCADE"),
                          nullable=False)
    comments = db.relationship(
        'Comment', backref='post_source', cascade="all,delete", lazy=True)

    def __init__(self, content, posted_by):
        self.content = content
        self.posted_by = posted_by

    def __repr__(self):
        return f'Post {self.id}'


class Comment(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    comment = db.Column(db.String(300), nullable=False)
    commented_on = db.Column(db.DateTime, default=datetime.utcnow)
    commented_by = db.Column(db.Integer, db.ForeignKey('user.id', ondelete="CASCADE", onupdate="CASCADE"),
                             nullable=False)
    post = db.Column(db.Integer, db.ForeignKey('post.id', ondelete="CASCADE", onupdate="CASCADE"),
                     nullable=False)

    def __init__(self, comment, commented_by, post):
        self.comment = comment
        self.commented_by = commented_by
        self.post = post

    def __repr__(self):
        return f'Comment {self.id}'

# schemas


class UserSchema(ma.Schema):
    class Meta:
        model = User
        fields = ('id', 'name', 'email')


user_schema = UserSchema()
users_schema = UserSchema(many=True)


class PostSchema(ma.Schema):
    class Meta:
        model = Post
        fields = ('id', 'content', 'posted_on', 'posted_by')


post_schema = PostSchema()
posts_schema = PostSchema(many=True)


class CommentSchema(ma.Schema):
    class Meta:
        model = Comment
        fields = ('id', 'comment', 'post', 'commented_by')


comment_schema = CommentSchema()
comments_schema = CommentSchema(many=True)
